# zntexteditor

Nouvel éditeur d'articles en JavaScript/Bootstrap pour le site www.zeden.net

## Installation

Vous pouvez cloner le projet en utilisant git clone

```bash
git clone https://gitlab.com/mateusbubono/zntexteditor.git
```

## Usage

Il vous suffit d'ouvrir le fichier index.html une fois le projet clôné ou de vous rendre directement à l'adresse suivante :

https://mateusbubono.gitlab.io/zntexteditor/