let regexMap = new Map()
regexMap.set(/((?<!WARNING : LES GROUPES DOIVENT SE COMPOSER DE 2 OU 3 IMAGES[\n\r])<collection[\s\w="'\-;:%]*>(?:(?:<a[\s\w\d="'.\/\-_;:\%#]*><img[\s\w\d="'.\/\-_;:\%#]*><\/a>){0,1}|(?:<img[\s\w\d="'.\/\-_;:\%#]*>){0,1}|(?:<img[\s\w\d="'.\/\-_;:\%#]*>){4,}|(?:<a[\s\w\d="'.\/\-_;:\%#]*><img[\s\w\d="'.\/\-_;:\%#]*><\/a>){4,})<\/collection>)/g, "WARNING : LES GROUPES DOIVENT SE COMPOSER DE 2 OU 3 IMAGES\n$1") //\s\w\d="'.\/\-_;:
regexMap.set(/(?<!<[\w\d=\/\."'\s\-_;:]+)\s+([.,]+)(?![\w\d=\/\."'\s\-_;:]+>)/g, "$1") // Suppression des espaces entre un mot et un point ou virgule
regexMap.set(/(?<!<[\w\d=\/\."'\s\-_;:]+)([.,]+)(\w+)(?![\w\d=\/\."'\s\-_;:]+>)/g, "$1 $2") //Ajout d'un espace entre un point et un mot ou virgule
regexMap.set(/(?<!<[\w\d=\/\."'\s\-_;:]+)([:;]+)(\w+)(?![\w\d=\/\."'\s\-_;:]+>)/g, "$1 $2") //Ajout d'un espace entre un : et un mot
regexMap.set(/(?<!<[\w\d=\/\."'\s\-_;:]+)(\w+)([:;]+)(?![\w\d=\/\."'\s\-_;:]+>)/g, "$1 $2") //Ajout d'un espace entre un mot et un :
regexMap.set(/(&nbsp)\s+(;)/g, "$1$2") //Ajout d'un espace entre un mot et un :
regexMap.set(/(\n{3,})/g, "\n\n") //Ajout d'un espace entre un mot et un :
//regexMap.set(/(\<[\w\/]+\>)\n*([\s\w]+)/g, "$1\n\n$2") //Ajout d'une ligne blanche entre une balise et du texte
//regexMap.set(/([\s\w]+\b)\n*(\<[\w\/]+\>)/g, "$1\n\n$2") //Ajout d'une ligne blanche entre du texte et une balise
regexMap.set(/(?<!<ul>[\s\n\r]*|<\/li>[\s\n\r]*)((?:<li>.*<\/li>[\n\s\r]*)+)(?![\n\s\r]*<\/ul>|[\n\s\r]*<li>)/g, "<ul>\n$1\n</ul>") //Ajout de <ul> </ul> autours de une ou plusieurs <li></li>
regexMap.set(/<LASUITE>/, "%LASUITE%") //La premi�re balise LASUITE est transform�e temporairement
regexMap.set(/<LASUITE>/g, "")  // Les autres Balises LASUITE sont supprimm�es
regexMap.set(/%LASUITE%/, "<LASUITE>")  // La premi�re balise LASUITE est remise en ordre
regexMap.set(/(?<!=\s*)"([\w\s]+)"/g, "«&nbsp;$1&nbsp;»")  //Remplacement des guillemets par  «&nbsp; sauf pour les guillemets entre balise

//regexMap.set(/(\<collection\>(?:\<img\s*[\w\d=\/\.\"\']*\>\<\/img\>)?|(?:\<img\s*[\w\d=\/\.\"\']*\>\<\/img\>){4,}\<\/collection\>)/g, "Warning : Les groupes doivent se composer de 2 ou 3 images\n$1") //Ajoute un warning si une collection ne possède pas le bon nombre d'image

let specialRegex = [
	[/\<ul\>[\s\n]*((\<li\>[\s\w\d\.]+\<\/li\>[\n\s]*)+)[\n\s]*\<\/ul\>/g,/((\<li\>[\s\w\d\.]+\<\/li\>[\n\s]*)+)/g, "<ul>\n$1\n</ul>"]
];



let commandButtons = document.querySelectorAll("button"); //#toolbar 
let articleRenderer = document.getElementById("article-renderer")
let textEditor = document.getElementById("text-editor")

let bGlobalVoidCommandInserted = false;

let pictureImporterInput = document.getElementById("picture-from-web");

for(let i=0; i<commandButtons.length; i++)
{
	commandButtons[i].addEventListener("mousedown", function (event)
	{
		let commandName = event.target.getAttribute("data-command")
		let commandAttribute = event.target.getAttribute("data-attribute")
		let textarea = document.getElementById("text-editor")
		let start = textarea.selectionStart;
		let end = textarea.selectionEnd;
		let textSelected = textarea.value.substring(start, end)
		let textToInsert = ""
		let tagSource = "#"
		
		textEditor.focus(); //Permet le focus dans l'�diteur quand on ferme l'importeur d'images
		
		if(commandName == "null")
		{
			return
		}

		if(commandName == "collection")
		{
			applyImageInsertion()
			return
		}

		if(commandName == "pretty-print")
		{
			applyPrettyPrintRules()
			return
		}
		
		if(commandName == "picture")
		{
			openImageInsertion()
			return
		}
		
		if(commandName == "close")
		{
			closeImageInsertion()
			return
		}

		if(searchForSourceCommand(commandName))
		{
			let userInput = getSourceFromUser()
			if(userInput == null)
			{
				return
			}

			tagSource = (userInput == "" ? "#":userInput)
		}

		if(!searchForSpecialCommand(commandName))
		{
			if(textSelected == "")
			{
				textSelected = "%TEXTTOINSERT%";
				bGlobalVoidCommandInserted  = true;
			}
			textToInsert = "<"+commandName+ (commandAttribute==null?"":" "+commandAttribute+"='"+tagSource+"' ")+">"+textSelected+"</"+commandName+">"  
			
			
		}
		else
		{
			textToInsert = commandName+textSelected
		}
		textarea.value = textarea.value.substr(0, start) + textToInsert + textarea.value.substr(end, textarea.value.length)
		renderArticle(null)
		
		//Positionnement du curseur à la suite de l'insertion d'une balise
		textEditor.selectionStart = start + textToInsert.length;
		textEditor.selectionEnd = textEditor.selectionStart;
		
	})
}


textEditor.addEventListener("keyup", renderArticle)
//let bGlobaleEnterPressed = false;

function renderArticle(event)
{
	if(articleRenderer != null && textEditor != null)
	{
		let textContent = textEditor.value
		textContent = textContent.replace(/\n/g, "<br/>")
		textContent = textContent.replace(/(\<\/\w+\>)\<br\/\>/g, "$1")
		articleRenderer.innerHTML = textContent
		
		
	}		
}

textEditor.addEventListener("blur", () => {
		setTimeout(() => {
				if(document.activeElement !== pictureImporterInput)
				{
					textEditor.focus();
					
					if(bGlobalVoidCommandInserted)
					{
						bGlobalVoidCommandInserted = false
						let cursorPosition = textEditor.value.indexOf("%TEXTTOINSERT%")
						textEditor.value = textEditor.value.replace(/%TEXTTOINSERT%/, "");
						renderArticle(null)
						textEditor.selectionStart = cursorPosition
						textEditor.selectionEnd = cursorPosition
					}
				}
			}, 0)
	})

function searchForSpecialCommand(commandName)
{
	
	specialCommandList = ["«&nbsp;","&nbsp;-&nbsp;", "&nbsp;»", "<LASUITE>" ]
	
	if(specialCommandList.indexOf(commandName) == -1)
	{
		return false
	}
	
	return true
}

function searchForSourceCommand(commandName)
{
	sourceCommandList = ["a", "video", "img"]
	
	if(sourceCommandList.indexOf(commandName) == -1)
	{
		return false
	}
	
	return true
}

function getSourceFromUser()
{
	return prompt("Entrer le lien de la source ici")
}


function applyPrettyPrintRules()
{
	//applySpecialPrettyPrintRules()
	for(let [regex, rule] of regexMap)
	{
		textEditor.value = textEditor.value.replace(regex, rule)
	}
	
	renderArticle(null)
}

function applySpecialPrettyPrintRules()
{
	for(let [firstRE, secondRE, ppRule] of specialRegex)
	{
		let firstREMatches = textEditor.value.match(firstRE)
		let secondREMatches = textEditor.value.match(secondRE)
		if(secondREMatches == null)
		{
			continue
		}
		for(let secondMatch of secondREMatches)
		{
			let bApplyRE = true;
			if(firstREMatches != null)
			{
				for(let firstMatch of firstREMatches)
				{
					if(firstMatch.indexOf(secondMatch) != -1)
					{
						bApplyRE = false;
						break;
					}
				}
			}
			
			
			if(bApplyRE)
			{
				textEditor.value = textEditor.value.replace(secondMatch, (secondMatch) => {
						ppRule = ppRule.replace("$1", secondMatch)
						return ppRule;
					});
			}
			
		}
	}
}

function applyImageInsertion()
{
	let inputPictureElement = null
	inputPictureElement = document.getElementById("picture-from-web")
	
	let collectionElement = treatWebPicture(inputPictureElement.value)
	if(collectionElement == null)
	{
		inputPictureElement = document.getElementById("picture-from-disk")
		collectionElement = treatDiskPicture(inputPictureElement.files)
		
		if(collectionElement == null)
		{
			return
		}
	}
	
	collectionElement.style.justifyContent = getPictureAlignment()
	
	//Insertion de la collection à la position courante du sélecteur
	textEditor.value = textEditor.value.substr(0, textEditor.selectionStart) + collectionElement.outerHTML + textEditor.value.substr(textEditor.selectionEnd, textEditor.value.length)
	
	inputPictureElement.value = ""
	
	closeImageInsertion()
	
	renderArticle(null)
}

function treatWebPicture(imgString)
{
	if(imgString == "" || imgString == null)
	{
		return null
	}	
	
	let imgArray = imgString.split(";")
	let collectionElement = document.createElement("collection")
	
	if(Object.keys(gAnchorElementObj).length > 0)
	{
		for(let key in gAnchorElementObj)
		{
			collectionElement.appendChild(gAnchorElementObj[key]) 
		}
	}
	else
	{
		for(let i=0 ; i<imgArray.length; i++)
		{
			collectionElement.appendChild(buildImgElement(imgArray[i]))
		}
	}
	
	return collectionElement
}

function treatDiskPicture(imgFileList)
{
	if(imgFileList == null || imgFileList.length == 0)
	{
		return null
	}

	let collectionElement = document.createElement("collection")
	for(let imgFile of imgFileList)
	{
		 collectionElement.appendChild(buildImgElement(imgFile.name))
	}
	
	return collectionElement
}

function buildImgElement(imgSource)
{
	let imgElement = document.createElement("img")
	imgElement.setAttribute("src", imgSource)
	
	return imgElement
}

function getPictureAlignment()
{
	let alignmentList = document.getElementsByName("picture-location")
	for(let alignment of alignmentList)
	{
		if(alignment.checked == true)
		{
			return alignment.value
		}
	}
	
	return "flex-start"
}

function openImageInsertion()
{
	//On vide la partie de crop des images importées
	let pictureToCrop = document.getElementById("picture-to-crop")
	pictureToCrop.innerHTML = ""
	let pictureRenderer = document.getElementById("picture-renderer")
	pictureRenderer.innerHTML = ""
	let pictureModifier = document.getElementById("picture-modifier")
	let buttonImgModifier = document.getElementById("picture-modifier-button")
	pictureModifier.innerHTML = ""
	pictureModifier.appendChild(buttonImgModifier)
	

	let imageInclusionElement = document.getElementById("picture-manager")
	imageInclusionElement.style.display = "block"
	
}

function closeImageInsertion()
{
	let imageInclusionElement = document.getElementById("picture-manager")
	imageInclusionElement.style.display = null

	gAnchorElementObj = {}
}


/*document.getElementById("couverture-picture-from-web").addEventListener("keyup", applyImageToCouvertureRenderer)
document.getElementById("couverture-picture-from-web").addEventListener("paste", applyImageToCouvertureRenderer)

function applyImageToCouvertureRenderer()
{
	
	const inputPicturePathOrUrl = getCouvertureInputPicture()

	const couverturePictureToCrop = document.getElementById("couverture-picture-to-crop")
	const couverturePictureRenderer = document.getElementById("couverture-picture-renderer")

	let imgNode = document.createElement("img");
	imgNode.src = inputPicturePathOrUrl;
	console.log(inputPicturePathOrUrl)

	couverturePictureToCrop.innerHTML = "";
	couverturePictureToCrop.appendChild(imgNode);


	const cropper = new Cropper(imgNode, {
						aspectRatio: 4 / 3,
						viewMode: 1,
		});
	//var canvas = cropper.getCroppedCanvas()

	//replacing the image url with base64 data
	//imgNode.src = canvas.toDataURL(); 

	
		document.getElementById("crop-button").addEventListener("click", () =>
		{
			let couverturePictureRenderer = document.getElementById("couverture-picture-renderer")
			console.log(couverturePictureRenderer)
			couverturePictureRenderer.innerHTML = "";
			couverturePictureRenderer.appendChild(cropper.getCroppedCanvas())
		})
	
		
}


function getCouvertureInputPicture()
{
	let inputPictureElement = null
	inputPictureElement = document.getElementById("couverture-picture-from-web")
	if(inputPictureElement == null || inputPictureElement.value == null || inputPictureElement.value == "")
	{
		inputPictureElement = document.getElementById("couverture-picture-from-disk")
		if(inputPictureElement == null || inputPictureElement.files == null || inputPictureElement.files.length == 0)
		{
			return ""
		}

		return inputPictureElement.files[0]
	}

	return inputPictureElement.value
}*/

//~~~~~~~~~~ CROP MANAGEMENT ~~~~~~~~~~

document.getElementById("picture-importer-button").addEventListener("click", getPicturesFromWebInput)

function getPicturesFromWebInput()
{
	//Récupérer le contenu de l'élément input contenant une adresse web
	let inputPictureElement = getFileOrUrlElement()
	if(inputPictureElement == null)
	{
		return
	}

	//Récupérer l'élément qui va contenir la liste des images à traiter
	let pictureModifierFieldset = document.getElementById("picture-modifier")

	//Récupérer le boutons qui va envoyer la liste des images à traités vers la section de modification des images
	let buttonImgModifier = document.getElementById("picture-modifier-button")
	buttonImgModifier.style.display = "block";

	//Séparation des différentes images dans un tableau
	const imgArr = inputPictureElement.value.split(";")

	//On efface le contenu du fieldset conteneur
	pictureModifierFieldset.innerHTML = ""
	
	//On ajoute pour chaque image un élément radio permettant de sélectionner une image particulière et de la modifier
	buildRadioList(imgArr, pictureModifierFieldset)

	//Le bouton modifier est ajouté à la fin
	pictureModifierFieldset.appendChild(buttonImgModifier)
}

//Récupérer le contenu de l'élément input contenant une adresse web ou un fichier
function getFileOrUrlElement()
{
	let inputPictureElement = document.getElementById("picture-from-web")
	if(inputPictureElement.value == "" || inputPictureElement == null)
	{
		//Si l'élément est vide regarder l'élément input contenant un fichier du disque
		inputPictureElement = document.getElementById("picture-from-disk")

		if(inputPictureElement.files == "" || inputPictureElement == null)
		{
			//Si l'élément est de nouveau vide on arrête la fonction
			return null
		}
	}

	return inputPictureElement
}

//On ajoute pour chaque image un élément radio permettant de sélectionner une image particulière et de la modifier
function buildRadioList(imgArr, parentContainer)
{
	for(let i = 0; i<imgArr.length; i++)
	{
		let radioElem = document.createElement("input")
		radioElem.type="radio"
		radioElem.value = imgArr[i]
		radioElem.setAttribute("id", "img-"+i)
		radioElem.name = "img-modifier"
		if(i == 0)
		{
			radioElem.checked = true
		}
		let labelElem = document.createElement("label")
		labelElem.htmlFor="img-"+i;
		labelElem.innerHTML = imgArr[i];

		parentContainer.appendChild(labelElem)
		parentContainer.appendChild(radioElem)

		gAnchorElementObj[imgArr[i]] = document.createElement("img")
		gAnchorElementObj[imgArr[i]].src = imgArr[i]
	}
}

document.getElementById("picture-modifier-button").addEventListener("click", openImageModification)


let gAnchorElementObj = {}
let gSelectedImgSrc = null
let gCropper = null

function openImageModification()
{
	//On efface l'aperçu précédent
	let pictureRenderer = document.getElementById("picture-renderer")
	pictureRenderer.innerHTML = ""
	//Récupérer le conteneur du croppeur et l'afficher
	let cropContainer = document.getElementById("crop-container")
	cropContainer.style.display = "block"

	//Récupérer dans la liste des images celle qui doit être modifier
	const radioElemArr = document.getElementsByName("img-modifier")
	
	//Récupérer dans la liste des images celle qui doit être modifiée
	let radioElemSelected = getRadioElementSelected()
	if(radioElemSelected == null)
	{
		return
	}

	//Récupérer la partie de la page destinée à contenir l'image à modifier
	const pictureToCrop = document.getElementById("picture-to-crop")

	//Récupérer la partie de la page destinée à contenir l'image après modification
	const couverturePictureRenderer = document.getElementById("picture-renderer")

	//Création de l'élément image qui va être modifiée via le cropper
	let imgNode = document.createElement("img");
	imgNode.src = radioElemSelected.value;

	//On efface le contenu précédent du cropper
	pictureToCrop.innerHTML = "";
	pictureToCrop.appendChild(imgNode);

	//gAnchorElementObj[imgNode.src] = imgNode
	gSelectedImgSrc = imgNode.src


	//Création du cropper avec un ratio prédéfini et un mode empêchant d'avoir un cadre plus grand que l'image
	const cropper = new Cropper(imgNode, {
						aspectRatio: 4 / 3,
						viewMode: 1,
		});

	gCropper = cropper
}

//Ajout d'un listener qui va ajouter l'image modifiée dans la partie rendu du crop-container
document.getElementById("crop-button").addEventListener("click", () =>
{
	let pictureRenderer = document.getElementById("picture-renderer")
	pictureRenderer.innerHTML = "";
	pictureRenderer.appendChild(gCropper.getCroppedCanvas())

	//On récupère et on check les éléments de la checkbox associée
	//La checkbox permet de savoir si l'application du cropped doit se faire sur la miniature de l'image et/ou sur l'image globale.
	const croppedImgCheckboxList = document.getElementsByName("cropped-image-application")
	for(const croppedImgCheckbox of croppedImgCheckboxList)
	{
		if(croppedImgCheckbox.checked)
		{
			//Si on applique à la miniature alors on crée un element d'ancrage et on lui ajoute l'image redimensionnée
			if(croppedImgCheckbox.id == "cropped-image-into-miniature-checkbox")
			{
				let anchorElement = document.createElement("a")
				anchorElement.href = "#";
				anchorElement.className = "img-miniature"
				let imgElement = document.createElement("img");
				//imgElement.src = gCropper.getCroppedCanvas().toDataURL();

				gCropper.getCroppedCanvas().toBlob((blob) => {
					imgElement.src  = URL.createObjectURL(blob);

					anchorElement.appendChild(imgElement)

					gAnchorElementObj[gSelectedImgSrc] = anchorElement
				})

			}
			else //Sinon on ajoute l'image redimensionnée pour qu'elle devienne l'image de base
			{
				/*let imgElement = document.createElement("img")
				imgElement.src = gCropper.getCroppedCanvas().toDataURL
				gAnchorElementList.push(null)
				gCanvasElementList.push(imgElement)*/
				//TODO
				console.log("");
			}
		}
	}
})	

function getRadioElementSelected()
{
	//Récupérer dans la liste des images celle qui doit être modifier
	const radioElemArr = document.getElementsByName("img-modifier")
	
	let radioElemSelected = null
	for(radioElem of radioElemArr)
	{
		if(radioElem.checked)
		{
			radioElemSelected = radioElem
			return radioElemSelected
		}
	}

	return null
}





